# crawling_tweets

Simple crawling with search and streaming

Implementation using library [Tweepy](http://docs.tweepy.org/en/latest/streaming_how_to.html)

>  pip install tweepy

`import tweepy`

<br>


# Search

`for tweet in tweepy.Cursor(api.search,q="#natuna",count=10, lang="id", since="2020-01-10").items():` 

<br>


# Stream

`stream = Stream(auth, StdOutListener())`

`stream.filter(track=['yogyakarta'])`